﻿namespace GF.DotNetControlInIE
{
    partial class DriveList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.listBox1 = new System.Windows.Forms.ListBox();
      this.label1 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // listBox1
      // 
      this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.listBox1.FormattingEnabled = true;
      this.listBox1.IntegralHeight = false;
      this.listBox1.ItemHeight = 19;
      this.listBox1.Location = new System.Drawing.Point(0, 23);
      this.listBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new System.Drawing.Size(251, 176);
      this.listBox1.TabIndex = 0;
      // 
      // label1
      // 
      this.label1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
      this.label1.Dock = System.Windows.Forms.DockStyle.Top;
      this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(251, 23);
      this.label1.TabIndex = 1;
      this.label1.Text = "My Drives";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // DriveList
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.listBox1);
      this.Controls.Add(this.label1);
      this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this.Name = "DriveList";
      this.Size = new System.Drawing.Size(251, 199);
      this.Load += new System.EventHandler(this.DriveList_Load);
      this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label1;
    }
}
